{{--
    $image => array // ['relative_url']
    $class => string // 'hero--childpage' if used on a childpage with left menu, and any additional classes
--}}
@if(isset($image) && is_array($image))
    <div class="hero__block{{ isset($class) ? ' ' . $class : '' }}" style="background-image: url('{{ $image['relative_url'] }}')"></div>
@endif
