@extends('partials.lend-content-area')

@section('content')
    <?php
    $promo1 = array_slice($lendtiles, 0, 1);

    $promo2 = array_slice($lendtiles, 1, 1);
    $promo3 = array_slice($lendtiles, 2, 1);
    $promo4 = array_slice($lendtiles, 3, 1);

    ?>

    <div class="ga-lend">

        <div class="row grid-row">
            <div class="large-6 columns  data-equalizer data-equalize-on=" medium
            "">
            <div class="row">
                <div class="large-12 columns grid-img port">
                    <a href="{{ $promo1[0]['link'] }}">
                        <img src="{{ $promo1[0]['relative_url'] }}"/>
                        <h3>{{ $promo1[0]['title'] }}</h3>
                    </a>
                </div>
            </div>
        </div>


        <div class="large-6 columns" data-equalizer-watch>
            <div class="row">
                <div class="large-6 columns grid-img">
                    <a href="{{ $promo2[0]['link'] }}">
                        <img src="{{ $promo2[0]['relative_url'] }}"/>
                        <h3>{{ $promo2[0]['title'] }}</h3>
                </div>
                <div class="large-6 columns grid-img">
                    <a href="{{ $promo3[0]['link'] }}">
                        <img src="{{ $promo3[0]['relative_url'] }}"/>
                        <h3>{{ $promo3[0]['title'] }}</h3>
                </div>
            </div>
            <div class="row">
                <div class="large-6 columns grid-img">
                    <a href="{{ $promo4[0]['link'] }}">
                        <img src="{{ $promo4[0]['relative_url'] }}"/>
                        <h3>{{ $promo4[0]['title'] }}</h3>
                </div>
                <div class="large-6 columns grid-img">
                    <a href="http://giving.wayne.edu/donate/ddi"><img src="img/donate.png"/></a>
                </div>
            </div>
        </div>
    </div>
    </div>



    <div class="lend-content">
        <div class="row">
            <div class="small-12 @if($site_menu['meta']['has_selected'] == false && ((isset($show_site_menu) && $show_site_menu != true) || !isset($show_site_menu)))xlarge-12 large-12 @else xlarge-9 large-9 @endif columns content"
                 data-off-canvas-content>
                <div class="xlarge-3 large-3 small-12 columns main-menu @if($site_menu['meta']['has_selected'] == false && ((isset($show_site_menu) && $show_site_menu != true) || !isset($show_site_menu))) hide-for-menu-top-up @endif off-canvas-absolute position-right"
                     id="mainMenu" data-off-canvas role="navigation">
                    @if(isset($site_menu_output) && isset($top_menu_output) && $site_menu !== $top_menu)
                        <div class="offcanvas-main-menu">
                            <ul>
                                <li>
                                    <a class="main-menu">Main Menu</a>

                                    {!! $top_menu_output !!}
                                </li>
                            </ul>
                        </div>
                    @endif

                    @if(isset($site_menu_output))
                        {!! $site_menu_output !!}
                    @endif

                    @yield('below_menu')

                    @if(isset($under_menu))
                        @include('components.image-list', ['images' => $under_menu, 'class' => 'under-menu'])
                    @endif
                </div>

                <div class="@if($site_menu['meta']['has_selected'] == false)large-12 @else large-9 @endif columns content">
                    @if(isset($hero) && $hero != false && $site_menu['meta']['has_selected'] == true)
                        @include('components.hero', ['image' => $hero, 'class' => 'hero--childpage'])
                    @endif

                    @if(isset($breadcrumbs) && count($breadcrumbs) > 0)
                        @include('partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
                    @endif

                    <h1 class="page-title">{{ $page['title'] }}</h1>

                    {!! $page['content']['main'] !!}
                </div>
            </div>
        </div>

        {{--<div class="row">--}}
            {{--<div class="news-col large-6 small-12 columns">--}}
                {{--<h2>News</h2>--}}

                {{--<dl class="listing">--}}
                    {{--@foreach($news as $items)--}}
                        {{--@foreach($items as $item)--}}
                            {{--<dt>--}}
                                {{--<a href="/news/{{ $item['slug'] }}-{{  $item['news_id'] }}">--}}
                                    {{--{{ $item['title'] }}--}}
                                {{--</a>--}}
                            {{--</dt>--}}
                        {{--@endforeach--}}
                    {{--@endforeach--}}
                {{--</dl>--}}
                {{--<a href="/news/" class="more-link">More news</a>--}}
            {{--</div>--}}
            {{--<div class="large-6 small-12 columns">--}}
                {{--<h2>Events</h2>--}}
                {{--<dl class="listing events">--}}
                    {{--@foreach($events as $items)--}}
                        {{--@foreach($items as $event)--}}
                            {{--<?php--}}
                            {{--$date = DateTime::createFromFormat( 'Y-m-d', $event['date'] )->format('M d');--}}
                            {{--$month = substr($date, 0,3);--}}
                            {{--$day = substr($date, -2, 2);--}}
                            {{--?>--}}
                            {{--<div class="an-event">--}}
                                {{--<dd>--}}
                                    {{--<time datetime="{{ $date }}">--}}
                                        {{--{{  $month }} <br> &nbsp; {{ $day }}--}}
                                    {{--</time>--}}
                                {{--</dd>--}}

                                {{--<dt><a href="{{ $event['url'] }}" class="spf-nolink" style="display:block; width:400px;">{{ $event['title'] }}</a></dt>--}}
                                {{--<div class="clearfix"></div>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--@endforeach--}}

                {{--</dl>--}}

                {{--<a href="//events.wayne.edu/main/month/" class="more-link spf-nolink">More events</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
@endsection
