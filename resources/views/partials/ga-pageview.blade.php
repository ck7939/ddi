@if(config('app.env') != 'local')
    <script>
        @if(config('app.ga_code') != 'UA-' && config('app.ga_name') != null)
        ga('{{ config('app.ga_name') }}.send', 'pageview', '{{ $parameters['path'] }}');
        @endif
        ga('allWayneState.send', 'pageview', '{{ $parameters['path'] }}');
    </script>
@endif
