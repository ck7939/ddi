@extends('layouts.lend')

@section('content-area')
    @yield('top')

    @yield('content')

    @if(isset($hero) && $hero != false && $site_menu['meta']['has_selected'] == false)
        @include('components.hero', ['image' => $hero])
    @endif

    @yield('bottom')
@endsection
