@extends('partials.content-area')

@section('below_menu')
    @include('components.news-categories', ['categories' => $news_categories, 'selected_category' => $selected_news_category])
@endsection

@section('content')
    <div class="ddi-content">
        <div class="row">
            <div class="small-12 @if($site_menu['meta']['has_selected'] == false && ((isset($show_site_menu) && $show_site_menu != true) || !isset($show_site_menu)))xlarge-12 large-12 @else xlarge-12 large-12 @endif columns content"
                 data-off-canvas-content>
                <div class="xlarge-3 large-3 small-12 columns main-menu @if($site_menu['meta']['has_selected'] == false && ((isset($show_site_menu) && $show_site_menu != true) || !isset($show_site_menu))) hide-for-menu-top-up @endif off-canvas-absolute position-right"
                     id="mainMenu" data-off-canvas role="navigation">
                    @if(isset($site_menu_output) && isset($top_menu_output) && $site_menu !== $top_menu)
                        <div class="offcanvas-main-menu">
                            <ul>
                                <li>
                                    <a class="main-menu">Main Menu</a>

                                    {!! $top_menu_output !!}
                                </li>
                            </ul>
                        </div>
                    @endif

                    @if(isset($site_menu_output))
                        {!! $site_menu_output !!}
                    @endif
                    @yield('below_menu')

                    @if(isset($under_menu))
                        @include('components.image-list', ['images' => $under_menu, 'class' => 'under-menu'])
                    @endif
                </div>

                <div class="@if($site_menu['meta']['has_selected'] == false)large-9 @else large-9 @endif columns content">
                    @if(isset($hero) && $hero != false && $site_menu['meta']['has_selected'] == true)
                        @include('components.hero', ['image' => $hero, 'class' => 'hero--childpage'])
                    @endif

                    @if(isset($breadcrumbs) && count($breadcrumbs) > 0)
                        @include('partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs])
                    @endif
                    <h1 class="page-title">{{ $page['title'] }}</h1>

                    <div class="news-listing">
                        <br>
                        <dl>
                            @foreach($news as $news_item)
                                <dt>

                                    <a href="/{{ ($site['subsite-folder'] !== null) ? $site['subsite-folder'] : '' }}news/{{ $news_item['slug'] }}-{{ $news_item['news_id'] }}">
                                        {{ $news_item['title'] }}
                                    </a>
                                </dt>

                                <dd>
                                    <time datetime="{{ $news_item['posted'] }}">{{ apdatetime(date('F j, Y', strtotime($news_item['posted']))) }}</time>
                                    {{ $news_item['excerpt'] }}
                                </dd>
                                <br>
                            @endforeach

                            @if(count($news) == 0)
                                <p>Currently there are no news
                                    items {{ isset($selected_news_category['category']) ? ' for the category ' . strtolower($selected_news_category['category']) : '' }}
                                    .</p>
                            @endif
                        </dl>

                        <div class="row">
                            <div class="xlarge-6 large-6 medium-6 small-6 columns">
                                @if(count($news) == $paging['perPage'])
                                    <p>
                                        <a href="{{ app('request')->url() }}?page={{ $paging['page_number_previous'] }}">&larr;
                                            Previous</a>
                                    </p>
                                @endif
                            </div>

                            <div class="xlarge-6 large-6 medium-6 small-6 columns text-right">
                                @if($paging['page_number_next'] >= 0)
                                    <p>
                                        <a href="{{ app('request')->url() }}?page={{ $paging['page_number_next'] }}">Next &rarr;</a>
                                    </p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


