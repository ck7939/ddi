import 'app';
import jQuery from 'jquery';
import 'slick-carousel/slick/slick.js';

(function($) {
    "use strict";

    /**
     * Slick module.
     */
    class Slick {

        constructor() {
            this._init();
        }

        /**
         * Initialize
         */
        _init() {
            // Slick Config
            var config = {
                dots: true,
                arrows: false
            };

            // Lazyload images
            $('img.b-lazy').each(function () {
                // Replace the data-src with the real src
                $(this).attr('src', $(this).attr('data-src'));
            });

            // Initialize Slick
            $('.rotate').slick(config);
        }
    }

    // Initialize
    var slick = new Slick();

    // Register this module
    window.WayneState.register('slick', slick);
})(jQuery);
