<?php
return [
    /*
    |--------------------------------------------------------------------------
    | NewRelic Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the application name that is set within New Relic to collect
    | metrics based on this application.
    |
    */
    'app_name' => 'ddi',
];
