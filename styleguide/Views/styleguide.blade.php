@extends('partials.content-area')

@section('content')
    <h1 class="page-title">{{ $page['title'] }}</h1>

    {{--<div class="row">--}}
        {{--<div class="medium-6 columns">--}}
            {{--@if(isset($news))--}}
                {{--@include('components.mini-news', ['news' => $news, 'url' => '/'.$site['subsite-folder'].'news'])--}}
            {{--@endif--}}
        {{--</div>--}}
        {{--<div class="medium-6 columns">--}}
            {{--@if(isset($events))--}}
                {{--@include('components.mini-events', ['events' => $events])--}}
            {{--@endif--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="row">--}}
        {{--<div class="medium-6 columns">--}}
            {{--@if(isset($minilist))--}}
                {{--@include('components.mini-list', ['items' => $minilist, 'url' => 'http://wayne.edu/', 'heading' => 'Mini List'])--}}
            {{--@endif--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="row">--}}
        {{--<div class="medium-6 columns">--}}
            {{--@if(isset($under_menu))--}}
                {{--<h2>Image List</h2>--}}

                {{--@include('components.image-list', ['images' => $under_menu, 'class' => 'image-list'])--}}
            {{--@endif--}}
        {{--</div>--}}

        {{--<div class="small-12 medium-6 columns">--}}
            {{--<h2>Image List Lazy</h2>--}}

            {{--@include('components.image-list-lazy', ['images' => $under_menu, 'class' => 'rotate'])--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<h2>Accordion</h2>--}}

    {{--@if(isset($events))--}}
        {{--@include('components.accordion', ['items' => $accordion])--}}
    {{--@endif--}}

    {{--<a class="button" onclick="$('pre.accordions').toggleClass('hide');">See Accordion Code</a>--}}

    {{--<pre class="accordions hide" style="background: #EAEAEA; margin-bottom: 10px;">--}}
    {{--{{ htmlspecialchars('--}}
{{--<ul class="accordion">--}}
    {{--<li class="is-active">--}}
        {{--<a href="#panel1a">Accordion 1</a>--}}
        {{--<div id="panel1a">--}}
            {{--<p>Panel 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
        {{--</div>--}}
    {{--</li>--}}
    {{--<li>--}}
        {{--<a href="#panel2a">Accordion 2</a>--}}
        {{--<div id="panel2a">--}}
            {{--<p>Panel 2. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
        {{--</div>--}}
    {{--</li>--}}
    {{--<li>--}}
        {{--<a href="#panel3a">Accordion 3</a>--}}
        {{--<div id="panel3a">--}}
            {{--<p>Panel 3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
        {{--</div>--}}
    {{--</li>--}}
{{--</ul>--}}
    {{--') }}--}}
    {{--</pre>--}}

    {{--<h2>Table Stack</h2>--}}

    {{--<table class="table-stack">--}}
    	{{--<thead>--}}
    		{{--<tr>--}}
    			{{--<th>First Name</th>--}}
    			{{--<th>Last Name</th>--}}
    			{{--<th>Email</th>--}}
    		{{--</tr>--}}
    	{{--</thead>--}}

    	{{--<tbody>--}}
            {{--@for ($i = 0; $i < 10; $i++)--}}
            {{--<tr valign="top">--}}
                {{--<td>{{ $faker->firstName }}</td>--}}
                {{--<td>{{ $faker->lastName }}</td>--}}
                {{--<td>{{ $faker->email }}</td>--}}
            {{--</tr>--}}
            {{--@endfor--}}
        {{--</tbody>--}}
    {{--</table>--}}

    {{--<a class="button" onclick="$('pre.table-stack').toggleClass('hide');">See Table Code</a>--}}

    {{--<pre class="table-stack hide" style="background: #EAEAEA; margin-bottom: 10px;">--}}
    {{--{{ htmlspecialchars('--}}
{{--<table class="table-stack">--}}
    {{--<thead>--}}
        {{--<tr>--}}
            {{--<th></th>--}}
            {{--<th></th>--}}
            {{--<th></th>--}}
        {{--</tr>--}}
    {{--</thead>--}}

    {{--<tbody>--}}
        {{--<tr valign="top">--}}
            {{--<td></td>--}}
            {{--<td></td>--}}
            {{--<td></td>--}}
        {{--</tr>--}}
    {{--</tbody>--}}
{{--</table>--}}
    {{--') }}--}}
    {{--</pre>--}}

    {{--<h2>Blockquote</h2>--}}

    {{--<blockquote>--}}
        {{--{{ $faker->paragraph(10) }}--}}
    {{--</blockquote>--}}

    {{--<h2>Maginific Pop-up</h2>--}}
    {{--<p><a href="//www.youtube.com/watch?v=guRgefesPXE"><img src="//i.wayne.edu/youtube/guRgefesPXE"></a></p>--}}

    {{--<h1>H1: {{ $faker->sentence }}</h1>--}}
    {{--<h2>H2: {{ $faker->sentence }}</h2>--}}
    {{--<h3>H3: {{ $faker->sentence }}</h3>--}}
    {{--<h4>H4: {{ $faker->sentence }}</h4>--}}
    {{--<h5>H5: {{ $faker->sentence }}</h5>--}}
    {{--<h6>H6: {{ $faker->sentence }}</h6>--}}
@endsection
