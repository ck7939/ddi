<?php

namespace Styleguide\Repositories;

use App\Repositories\PromoRepository as Repository;

class PromoRepository extends Repository
{
    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $data)
    {
        // Only pull under_menu promos if they match the page_ids that are specified
        $under_menu = in_array($data['page']['id'], [100, 101100]) ? app('Factories\UnderMenu')->create(2) : null;

        // Only pull hero promos if they match the pages_ids that are specificed
        $hero = in_array($data['page']['id'], [101100, 101101]) ? app('Factories\HeroImage')->create(1) : null;

        // Only pull accordion for childpage template
        $accordion = in_array($data['page']['id'], [101100]) ? app('Factories\Accordion')->create(5) : null;

        return [
            // Contact footer
            'contact' => app('Factories\FooterContact')->create(1),

            // Social footer
            'social' => app('Factories\FooterSocial')->create(7),

            // Hero
            'hero' => $hero,

            // Under menu
            'under_menu' => $under_menu,

            // Accordion child page
            'accordion_page' => $accordion,
        ];
    }
}
