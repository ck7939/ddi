<?php

namespace Styleguide\Http\Controllers;

use Contracts\Repositories\NewsRepositoryContract;
use Contracts\Repositories\EventRepositoryContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Faker\Factory;

class StyleGuideController extends Controller
{
    /**
     * @param NewsRepositoryContract $news
     * @param EventRepositoryContract $events
     * @param Factory $fake
     */
    public function __construct(NewsRepositoryContract $news, EventRepositoryContract $events, Factory $faker)
    {
        $this->news = $news;
        $this->events = $events;
        $this->faker['faker'] = $faker->create();
    }

    /**
     * View the styleguide.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get the news
        $news = $this->news->getNewsByDisplayOrder($request->data['site']['id']);

        // Get the events
        $events = $this->events->getEvents($request->data['site']['id']);

        // Get accordion
        $promos['accordion'] = app('Factories\Accordion')->create(5);

        // Get minilist
        $promos['minilist'] = app('Factories\MiniList')->create(4);

        // Show the view
        return view('styleguide', merge($request->data, $this->faker, $news, $events, $promos));
    }
}
