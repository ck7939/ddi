<?php

namespace Styleguide\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Faker\Factory;

class HeaderTitleDoubleController extends Controller
{
    /**
     * @param Factory $fake
     */
    public function __construct(Factory $faker)
    {
        $this->faker = $faker->create();
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Set a sub title
        config(['app.sub_title' => $this->faker->sentence($this->faker->numberBetween(2, 4))]);

        // Show the view
        return view('styleguide-childpage', merge($request->data));
    }
}
