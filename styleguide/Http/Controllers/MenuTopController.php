<?php

namespace Styleguide\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Styleguide\Repositories\MenuRepository;

class MenuTopController extends Controller
{
    /**
     * @param MenuRepository $menuRepository
     */
    public function __construct(MenuRepository $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Enable top menu
        config(['app.top_menu_enabled' => true]);

        // Only get a few items to show for top menu
        $top_menu = array_slice($request->data['top_menu']['menu'], 0, 4, true);

        // Parse the top menu again to override the to menu output
        $request->data['top_menu_output'] = $this->menuRepository->getTopMenuOutput($top_menu);

        // Show the view
        return view('styleguide-childpage', merge($request->data));
    }
}
