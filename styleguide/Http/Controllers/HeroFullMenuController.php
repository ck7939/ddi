<?php

namespace Styleguide\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HeroFullMenuController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get a hero image
        $request->data['hero'] = app('Factories\HeroImage')->create(1);

        // Show the view
        return view('styleguide-childpage', merge($request->data));
    }
}
