<?php

namespace Styleguide\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MenuLeftController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Disable top menu
        config(['app.top_menu_enabled' => false]);

        // Show the view
        return view('styleguide-childpage', merge($request->data));
    }
}
