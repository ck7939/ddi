<?php

namespace Styleguide\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FooterContactThreeController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Override the contact footer
        $request->data['contact'] = app('Factories\FooterContact')->create(3);

        // Show the view
        return view('styleguide-childpage', merge($request->data));
    }
}
