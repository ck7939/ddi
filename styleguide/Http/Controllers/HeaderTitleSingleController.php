<?php

namespace Styleguide\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HeaderTitleSingleController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // No sub title
        config(['app.sub_title' => null]);

        // Show the view
        return view('styleguide-childpage', merge($request->data));
    }
}
