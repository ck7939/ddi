<?php

namespace Styleguide\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FooterContactOneController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get the news
        $request->data['contact'] = app('Factories\FooterContact')->create(1);

        // Show the view
        return view('styleguide-childpage', merge($request->data));
    }
}
