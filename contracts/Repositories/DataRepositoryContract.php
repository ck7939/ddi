<?php

namespace Contracts\Repositories;

interface DataRepositoryContract
{
    /**
     * Get data and send it with the request object.
     */
    public function getRequestData(array $data);
}
