<?php

namespace Contracts\Repositories;

interface PromoRepositoryContract
{
    /**
     * Homepage Promotions.
     *
     * @return array
     */
    public function getHomepagePromos();
}
