<?php if(config('app.env') != 'local'): ?>
    <script>
        <?php if(config('app.ga_code') != 'UA-' && config('app.ga_name') != null): ?>
        ga('<?php echo e(config('app.ga_name')); ?>.send', 'pageview', '<?php echo e($parameters['path']); ?>');
        <?php endif; ?>
        ga('allWayneState.send', 'pageview', '<?php echo e($parameters['path']); ?>');
    </script>
<?php endif; ?>
