<?php if(isset($categories) && count($categories) > 0): ?>
    <h2>Filter by category</h2>

    <ul>
        <?php foreach($categories as $category): ?>
            <li<?php echo $selected_category['category_id'] == $category['category_id'] ? ' class="selected"': ''; ?>><a href="/<?php echo e(($site['subsite-folder'] !== null) ? $site['subsite-folder'] : ''); ?>news/category/<?php echo e($category['slug']); ?>"><?php echo e($category['category']); ?></a></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
