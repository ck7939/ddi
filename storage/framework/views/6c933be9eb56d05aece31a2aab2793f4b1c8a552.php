<?php $__env->startSection('content'); ?>
<?php
$promo1 = array_slice($dditiles, 0, 1);

$promo2 = array_slice($dditiles, 1, 1);
$promo3 = array_slice($dditiles, 2, 1);
$promo4 = array_slice($dditiles, 3, 1);

?>

    <div class="ga">

                <div class="row grid-row">
                    <div class="large-6 columns  data-equalizer data-equalize-on=" medium
                    "">
                    <div class="row">
                        <div class="large-12 columns grid-img port">
                            <a href="<?php echo e($promo1[0]['link']); ?>">
                            <img src="<?php echo e($promo1[0]['relative_url']); ?>"/>
                            <h3><?php echo e($promo1[0]['title']); ?></h3>
                            </a>
                        </div>
                    </div>
                </div>


        <div class="large-6 columns" data-equalizer-watch>
            <div class="row">
                <div class="large-6 columns grid-img">
                    <a href="<?php echo e($promo2[0]['link']); ?>">
                        <img src="<?php echo e($promo2[0]['relative_url']); ?>"/>
                        <h3><?php echo e($promo2[0]['title']); ?></h3>
                </div>
                <div class="large-6 columns grid-img">
                    <a href="<?php echo e($promo3[0]['link']); ?>">
                        <img src="<?php echo e($promo3[0]['relative_url']); ?>"/>
                        <h3><?php echo e($promo3[0]['title']); ?></h3>
                </div>
            </div>
            <div class="row">
                <div class="large-6 columns grid-img">
                    <a href="<?php echo e($promo4[0]['link']); ?>">
                        <img src="<?php echo e($promo4[0]['relative_url']); ?>"/>
                        <h3><?php echo e($promo4[0]['title']); ?></h3>
                </div>
                <div class="large-6 columns grid-img">
                    <img src="img/donate.png"/>
                </div>
            </div>
        </div>
    </div>
    </div>



    <div class="ddi-content">
        <div class="row">
            <div class="small-12 <?php if($site_menu['meta']['has_selected'] == false && ((isset($show_site_menu) && $show_site_menu != true) || !isset($show_site_menu))): ?>xlarge-12 large-12 <?php else: ?> xlarge-9 large-9 <?php endif; ?> columns content"
                 data-off-canvas-content>
                <div class="xlarge-3 large-3 small-12 columns main-menu <?php if($site_menu['meta']['has_selected'] == false && ((isset($show_site_menu) && $show_site_menu != true) || !isset($show_site_menu))): ?> hide-for-menu-top-up <?php endif; ?> off-canvas-absolute position-right"
                     id="mainMenu" data-off-canvas role="navigation">
                    <?php if(isset($site_menu_output) && isset($top_menu_output) && $site_menu !== $top_menu): ?>
                        <div class="offcanvas-main-menu">
                            <ul>
                                <li>
                                    <a class="main-menu">Main Menu</a>

                                    <?php echo $top_menu_output; ?>

                                </li>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <?php if(isset($site_menu_output)): ?>
                        <?php echo $site_menu_output; ?>

                    <?php endif; ?>

                    <?php echo $__env->yieldContent('below_menu'); ?>

                    <?php if(isset($under_menu)): ?>
                        <?php echo $__env->make('components.image-list', ['images' => $under_menu, 'class' => 'under-menu'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                </div>

                <div class="<?php if($site_menu['meta']['has_selected'] == false): ?>large-12 <?php else: ?> large-9 <?php endif; ?> columns content">
                    <?php if(isset($hero) && $hero != false && $site_menu['meta']['has_selected'] == true): ?>
                        <?php echo $__env->make('components.hero', ['image' => $hero, 'class' => 'hero--childpage'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>

                    <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0): ?>
                        <?php echo $__env->make('partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>

                    <h1 class="page-title"><?php echo e($page['title']); ?></h1>

                    <?php echo $page['content']['main']; ?>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="news-col large-6 small-12 columns">
                <h2>News</h2>

                <dl class="listing">
                    <?php foreach($news as $items): ?>
                        <?php foreach($items as $item): ?>
                    <dt>
                        <a href="/news/<?php echo e($item['slug']); ?>-<?php echo e($item['news_id']); ?>">
                            <?php echo e($item['title']); ?>

                        </a>
                    </dt>
                            <?php endforeach; ?>
                    <?php endforeach; ?>
                </dl>
                <a href="/news/" class="more-link">More news</a>
            </div>
            <div class="large-6 small-12 columns">
                <h2>Events</h2>
                <dl class="listing events">
                <?php foreach($events as $items): ?>
                    <?php foreach($items as $event): ?>
                            <?php
                            $date = DateTime::createFromFormat( 'Y-m-d', $event['date'] )->format('M d');
                            $month = substr($date, 0,3);
                            $day = substr($date, -2, 2);
                            ?>
                                <div class="an-event">
                                <dd>
                                    <time datetime="<?php echo e($date); ?>">
                                       <?php echo e($month); ?> <br> &nbsp; <?php echo e($day); ?>

                                    </time>
                                </dd>

                                <dt><a href="<?php echo e($event['url']); ?>" class="spf-nolink" style="display:block; width:400px;"><?php echo e($event['title']); ?></a></dt>
                                <div class="clearfix"></div>
                            </div>
                        <?php endforeach; ?>
                <?php endforeach; ?>

                </dl>

                <a href="//events.wayne.edu/main/month/" class="more-link spf-nolink">More events</a>
            </div>
        </div>
        </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('partials.content-area', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>