<?php $__env->startSection('content'); ?>
    <div class="lend-content">
        <div class="row">
            <div class="small-12 <?php if($site_menu['meta']['has_selected'] == false && ((isset($show_site_menu) && $show_site_menu != true) || !isset($show_site_menu))): ?>xlarge-12 large-12 <?php else: ?> xlarge-12 large-12 <?php endif; ?> columns content" data-off-canvas-content>
                <div class="xlarge-3 large-3 small-12 columns main-menu <?php if($site_menu['meta']['has_selected'] == false && ((isset($show_site_menu) && $show_site_menu != true) || !isset($show_site_menu))): ?> hide-for-menu-top-up <?php endif; ?> off-canvas-absolute position-right" id="mainMenu" data-off-canvas role="navigation">
                    <?php if(isset($site_menu_output) && isset($top_menu_output) && $site_menu !== $top_menu): ?>
                        <div class="offcanvas-main-menu">
                            <ul>
                                <li>
                                    <a class="main-menu">Main Menu</a>

                                    <?php echo $top_menu_output; ?>

                                </li>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <?php if(isset($site_menu_output)): ?>
                        <?php echo $site_menu_output; ?>

                    <?php endif; ?>

                    <?php echo $__env->yieldContent('below_menu'); ?>

                    <?php if(isset($under_menu)): ?>
                        <?php echo $__env->make('components.image-list', ['images' => $under_menu, 'class' => 'under-menu'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                </div>

                <div class="<?php if($site_menu['meta']['has_selected'] == false): ?>large-12 <?php else: ?> large-9 <?php endif; ?> columns content">
                    <?php if(isset($hero) && $hero != false && $site_menu['meta']['has_selected'] == true): ?>
                        <?php echo $__env->make('components.hero', ['image' => $hero, 'class' => 'hero--childpage'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>

                    <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0): ?>
                        <?php echo $__env->make('partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                    <h1 class="page-title"><?php echo e($page['title']); ?></h1>

                    <?php echo $page['content']['main']; ?>


                    <?php if(isset($accordion_page) && count($accordion_page) > 0): ?>
                        <?php echo $__env->make('components.accordion', ['items' => $accordion_page], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                </div></div></div></div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('partials.lend-content-area', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>