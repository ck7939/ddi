<?php /*
    $social => array // [['title','link']]
*/ ?>
<?php if(is_array($social)): ?>
    <div class="footer-social">
        <div class="row">
            <div class="small-12 text-center columns">
                <ul>
                    <?php foreach($social as $item): ?>
                        <li>
                            <a href="<?php echo e($item['link']); ?>" target="_blank" rel="noopener">
                                <i class="icon-<?php echo e(strtolower($item['title'])); ?>"><span><?php echo e($item['title']); ?></span></i>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
<?php endif; ?>
