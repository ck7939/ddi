<?php /*
    $breadcrumbs => array // [['display_name', 'relative_url']]
*/ ?>
<nav role="navigation" class="breadcrumbs">
    <ul class="breadcrumbs">
        <?php foreach($breadcrumbs as $key=>$crumb): ?>
            <?php if($key == 0): ?>
                <li class="first">
                    <a href="/" title="<?php echo e(strip_tags($crumb['display_name'])); ?>"><span class="icon-home"></span><span class="text"><?php echo e(strip_tags($crumb['display_name'])); ?></span></a>
                    <span class="icon-right-open-mini"></span>
            <?php elseif($key == (count($breadcrumbs) - 1)): ?>
                <li class="last">
                    <?php echo e($crumb['display_name']); ?>

            <?php else: ?>
                <li>
                <a href="<?php echo e($crumb['relative_url']); ?>" title="<?php echo e(strip_tags($crumb['display_name'])); ?>"><?php echo e($crumb['display_name']); ?></a>
                <span class="icon-right-open-mini"></span>
            <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
</nav>