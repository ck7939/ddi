<?php /*
    $image => array // ['relative_url']
    $class => string // 'hero--childpage' if used on a childpage with left menu, and any additional classes
*/ ?>
<?php if(isset($image) && is_array($image)): ?>
    <div class="hero__block<?php echo e(isset($class) ? ' ' . $class : ''); ?>" style="background-image: url('<?php echo e($image['relative_url']); ?>')"></div>
<?php endif; ?>
