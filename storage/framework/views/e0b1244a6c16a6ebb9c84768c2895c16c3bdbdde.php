<?php /*
    $images => array // [['link', 'title', 'relative_url']]
    $class => string // 'under-menu'
*/ ?>
<?php if(is_array($images)): ?>
    <?php if(isset($class)): ?><div class="<?php echo e($class); ?>"><?php endif; ?>

    <?php foreach($images as $image): ?>
        <div>
            <?php if($image['link'] != ''): ?><a href="<?php echo e($image['link']); ?>"><?php endif; ?>
                <img src="<?php echo e($image['relative_url']); ?>" alt="<?php echo e($image['title']); ?>" />
            <?php if($image['link'] != ''): ?></a><?php endif; ?>
        </div>
    <?php endforeach; ?>

    <?php if(isset($class)): ?></div><?php endif; ?>
<?php endif; ?>