<?php $__env->startSection('content'); ?>
    <?php if($back_url != ''): ?>
        <div class="row profile__return">
            <div class="columns small-12">
                <a href="<?php echo e($back_url); ?>">&lt; Return to listing</a>
            </div>
        </div>
    <?php endif; ?>

    <div class="row profile__block">
        <div class="small-12 large-4 columns">
            <?php if(isset($profile['data']['Picture']['url'])): ?>
                <img src="<?php echo e($profile['data']['Picture']['url']); ?>" alt="<?php echo e($page['title']); ?>" class="profile__img">
            <?php else: ?>
                <img src="/_resources/images/no-photo.svg" alt="<?php echo e($page['title']); ?>" class="profile__img">
            <?php endif; ?>

            <h1 class="hide-for-large page-title"><?php echo e($page['title']); ?></h1>

            <div class="profile--contact-info">
                <?php if(isset($profile['data']['Title'])): ?>
                    <p class="title"><?php echo e($profile['data']['Title']); ?></p>
                <?php endif; ?>

                <?php foreach($profile['data'] as $field=>$data): ?>
                    <?php if(in_array($field, $contact_fields)): ?>
                        <?php if(in_array($field, $file_fields)): ?>
                            <p><a href="<?php echo e($data['url']); ?>"><?php echo e($field); ?></a></p>
                        <?php else: ?>
                            <?php if(is_array($data)): ?>
                                <?php foreach($data as $value): ?>
                                    <?php if(in_array($field, $url_fields)): ?>
                                        <p><a href="<?php echo e($value); ?>"><?php echo e($value); ?></a></p>
                                    <?php else: ?>
                                        <p><?php echo e($value); ?></p>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <?php if($field == 'Email'): ?>
                                    <p><a href="mailto:<?php echo e($data); ?>"><?php echo e($data); ?></a></p>
                                <?php elseif($field == 'Fax'): ?>
                                    <p><?php echo strip_tags($data); ?> (fax)</p>
                                <?php elseif(in_array($field, $url_fields)): ?>
                                    <p><a href="<?php echo e($data); ?>"><?php echo e($data); ?></a></p>
                                <?php else: ?>
                                    <p><?php echo strip_tags($data); ?></p>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
             </div>
        </div>

        <div class="small-12 large-8 columns">
            <h1 class="page-title show-for-large"><?php echo e($page['title']); ?></h1>

            <?php foreach($profile['data'] as $field=>$data): ?>
                <?php if(!in_array($field, $contact_fields) && !in_array($field, $hidden_fields)): ?>
                    <h2><?php echo e($field); ?></h2>

                    <?php if(is_array($data)): ?>
                        <?php foreach($data as $value): ?>
                            <?php echo $value; ?>

                        <?php endforeach; ?>
                    <?php else: ?>
                        <?php echo $data; ?>

                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('partials.content-area', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>