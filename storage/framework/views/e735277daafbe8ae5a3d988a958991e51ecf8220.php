<?php /*
    $site => array // ['title']
    $top_menu_output => string // '<ul></ul>'
*/ ?>
<div class="menu-top">
    <div class="menu-top-container menu-top-bg">
        <div class="row">
            <div class="small-12 columns">
                <div class="vertical-centering title-area">
                    <a href="/">
                    <img src="/img/ddi-logo.png" alt="" class="float-left">
                    <h2 class="float-right">
                            Michigan Developmental Disabilities Institute
                            <?php /*<?php if(config('app.sub_title') !== null): ?><span><?php echo e(config('app.sub_title')); ?></span><?php endif; ?>*/ ?>
                            <?php /*<?php echo e($site['title']); ?>*/ ?>
                    </h2>
                    </a>

                </div>

                <div class="float-right vertical-centering">
                    <div>
                        <?php if(config('app.top_menu_enabled') == true): ?>
                            <section id="top-menu">
                                <?php echo $top_menu_output; ?>

                            </section>
                        <?php endif; ?>

                        <div>
                            <ul class="menu-top menu-button hide-for-menu-top-up">
                                <li><a href="#mainMenu" class="menu-toggle menu-icon" data-toggle="mainMenu"><span>Menu</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="menu-top-placeholder"></div>
</div>
