<?php if(config('app.env') != 'local'): ?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    <?php if(config('app.ga_code') != 'UA-' && config('app.ga_name') != null): ?>ga('create', '<?php echo e(config('app.ga_code')); ?>', 'wayne.edu', {'name': '<?php echo e(config('app.ga_name')); ?>'});<?php endif; ?>
    <?php if(config('app.ga_all_wsu') == true): ?>ga('create', 'UA-35684592-1', 'wayne.edu', {'name': 'allWayneState'});<?php endif; ?>
</script>
<?php endif; ?>

<?php echo $__env->make('partials.ga-pageview', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
