<?php /*
    $contact => array // [['title', 'link', 'description']]
*/ ?>
<?php if(is_array($contact) && count($contact) > 0): ?>
    <div class="footer-contact">
        <?php if(count($contact) == 1): ?>
            <div class="row">
                <div class="columns small-12 text-center">
                    <?php foreach($contact as $info): ?>
                        <h3>
                            <?php if($info['link'] != ''): ?><a href="<?php echo e($info['link']); ?>"><?php endif; ?>
                            <?php echo e($info['title']); ?>

                            <?php if($info['link'] != ''): ?></a><?php endif; ?>
                        </h3>

                        <?php echo $info['description']; ?>

                    <?php endforeach; ?>
                </div>
            </div>
        <?php else: ?>
            <div class="row" data-equalizer>
                <?php foreach($contact as $info): ?>
                    <div class="columns small-12 large-4<?php echo e(($info == end($contact)) ? ' end' : ''); ?>" data-equalizer-watch>
                        <?php echo $info['description']; ?>


                        <hr />
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
