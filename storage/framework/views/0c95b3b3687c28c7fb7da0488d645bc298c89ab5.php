)

<?php $__env->startSection('content-area'); ?>
    <?php echo $__env->yieldContent('top'); ?>

    <?php echo $__env->yieldContent('content'); ?>

    <?php if(isset($hero) && $hero != false && $site_menu['meta']['has_selected'] == false): ?>
        <?php echo $__env->make('components.hero', ['image' => $hero], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    <?php echo $__env->yieldContent('bottom'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.lend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>