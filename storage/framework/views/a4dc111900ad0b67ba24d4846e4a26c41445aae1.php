<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="Keywords" content="<?php echo e($page['keywords']); ?>" />
    <meta name="Description" content="<?php echo e($page['description']); ?>" />
    <meta http-equiv="last-modified" content="<?php echo e($page['updated-at']); ?>" />
    <meta name="Author" content="Wayne State University Web Communications" />
    <title><?php echo $__env->make('partials.head-title', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?></title>

    <link rel="stylesheet" href="<?php echo e(elixir('css/main.css')); ?>" />
    <link href="//fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/js/vendor/modernizr.js"></script>
    <!--[if lt IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
    <?php echo $__env->make('partials.ga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <meta class="foundation-mq">
</head>
<body>

<?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div id="menu-top-section" class="header-menu">
    <?php echo $__env->make('partials.menu-top', ['site' => $site], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>

<div class="off-canvas-wrapper">
    <div id="content">
        <?php echo $__env->yieldContent('content-area'); ?>
    </div>

    <div id="footer-social">
        <?php if(isset($social) && count($social) > 0): ?>
            <?php echo $__env->make('partials.footer-social', ['social' => $social], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
    </div>

    <div id="footer-contact">
        <?php if(isset($contact) && count($contact) > 0): ?>
            <?php echo $__env->make('partials.footer-contact', ['contact' => $contact], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
    </div>
</div>

<?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="<?php echo e(elixir('js/main.js')); ?>"></script>
<script type="text/javascript" src="//acc.magixite.com/license/la?litk=2vsxfnqdf9ee9udi"></script>

</body>
</html>
