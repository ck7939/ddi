<?php $__env->startSection('content'); ?>
    <div class="ddi-content">
        <div class="row">
            <div class="small-12 <?php if($site_menu['meta']['has_selected'] == false && ((isset($show_site_menu) && $show_site_menu != true) || !isset($show_site_menu))): ?>xlarge-12 large-12 <?php else: ?> xlarge-12 large-12 <?php endif; ?> columns content" data-off-canvas-content>
                <div class="xlarge-3 large-3 small-12 columns main-menu <?php if($site_menu['meta']['has_selected'] == false && ((isset($show_site_menu) && $show_site_menu != true) || !isset($show_site_menu))): ?> hide-for-menu-top-up <?php endif; ?> off-canvas-absolute position-right" id="mainMenu" data-off-canvas role="navigation">
                    <?php if(isset($site_menu_output) && isset($top_menu_output) && $site_menu !== $top_menu): ?>
                        <div class="offcanvas-main-menu">
                            <ul>
                                <li>
                                    <a class="main-menu">Main Menu</a>

                                    <?php echo $top_menu_output; ?>

                                </li>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <?php if(isset($site_menu_output)): ?>
                        <?php echo $site_menu_output; ?>

                    <?php endif; ?>

                    <?php echo $__env->yieldContent('below_menu'); ?>

                    <?php if(isset($under_menu)): ?>
                        <?php echo $__env->make('components.image-list', ['images' => $under_menu, 'class' => 'under-menu'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                </div>

                <div class="<?php if($site_menu['meta']['has_selected'] == false): ?>large-12 <?php else: ?> large-9 <?php endif; ?> columns content">
                    <?php if(isset($hero) && $hero != false && $site_menu['meta']['has_selected'] == true): ?>
                        <?php echo $__env->make('components.hero', ['image' => $hero, 'class' => 'hero--childpage'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>

                    <?php if(isset($breadcrumbs) && count($breadcrumbs) > 0): ?>
                        <?php echo $__env->make('partials.breadcrumbs', ['breadcrumbs' => $breadcrumbs], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
    <h1 class="page-title"><?php echo e($page['title']); ?></h1>

    <?php if($hide_filtering == false): ?>
        <form name="programs" method="get" class="filter">
            <div class="row">
                <div class="large-12 columns">
                    <label for="program">View by department:</label>
                    <div class="row collapse">
                        <div class="small-10 columns">
                            <select name="group">
                                <?php foreach($dropdown_groups as $key=>$value): ?>
                                    <option value="<?php echo e($key); ?>"<?php if($key == $selected_group): ?> selected="selected"<?php endif; ?>><?php echo e($value); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="small-2 columns">
                            <input type="submit" value="Filter" class="postfix button" />
                        </div>
                    </div>
                </div>
            </div>
        </form>
    <?php endif; ?>

    <div class="row small-up-2 medium-up-3">
        <?php $__empty_1 = true; foreach((array)$profiles as $profile): $__empty_1 = false; ?>
            <div class="columns profile">
                <a href="/<?php echo e(($site['subsite-folder'] !== null) ? $site['subsite-folder'] : ''); ?>profile/<?php echo e($profile['data']['AccessID']); ?>" class="profile-img" style="background-image: url('<?php echo e(isset($profile['data']['Picture']['url']) ? $profile['data']['Picture']['url'] : '/_resources/images/no-photo.svg'); ?>');" alt="<?php echo e($profile['data']['First Name']); ?> <?php echo e($profile['data']['Last Name']); ?>"></a>

                <a href="/<?php echo e(($site['subsite-folder'] !== null) ? $site['subsite-folder'] : ''); ?>profile/<?php echo e($profile['data']['AccessID']); ?>"><?php echo e($profile['data']['First Name']); ?> <?php echo e($profile['data']['Last Name']); ?></a>

                <?php if(isset($profile['data']['Title'])): ?>
                    <span><?php echo e($profile['data']['Title']); ?></span>
                <?php endif; ?>
            </div>
        <?php endforeach; if ($__empty_1): ?>
            <p>No profiles found.</p>
        <?php endif; ?>
    </div>
                </div></div></div></div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('partials.content-area', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>