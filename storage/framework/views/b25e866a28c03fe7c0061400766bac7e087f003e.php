<?php $__env->startSection('content'); ?>
    <h1 class="page-title"><?php echo e($page['title']); ?></h1>

    <?php /*<div class="row">*/ ?>
        <?php /*<div class="medium-6 columns">*/ ?>
            <?php /*<?php if(isset($news)): ?>*/ ?>
                <?php /*<?php echo $__env->make('components.mini-news', ['news' => $news, 'url' => '/'.$site['subsite-folder'].'news'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>*/ ?>
            <?php /*<?php endif; ?>*/ ?>
        <?php /*</div>*/ ?>
        <?php /*<div class="medium-6 columns">*/ ?>
            <?php /*<?php if(isset($events)): ?>*/ ?>
                <?php /*<?php echo $__env->make('components.mini-events', ['events' => $events], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>*/ ?>
            <?php /*<?php endif; ?>*/ ?>
        <?php /*</div>*/ ?>
    <?php /*</div>*/ ?>

    <?php /*<div class="row">*/ ?>
        <?php /*<div class="medium-6 columns">*/ ?>
            <?php /*<?php if(isset($minilist)): ?>*/ ?>
                <?php /*<?php echo $__env->make('components.mini-list', ['items' => $minilist, 'url' => 'http://wayne.edu/', 'heading' => 'Mini List'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>*/ ?>
            <?php /*<?php endif; ?>*/ ?>
        <?php /*</div>*/ ?>
    <?php /*</div>*/ ?>

    <?php /*<div class="row">*/ ?>
        <?php /*<div class="medium-6 columns">*/ ?>
            <?php /*<?php if(isset($under_menu)): ?>*/ ?>
                <?php /*<h2>Image List</h2>*/ ?>

                <?php /*<?php echo $__env->make('components.image-list', ['images' => $under_menu, 'class' => 'image-list'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>*/ ?>
            <?php /*<?php endif; ?>*/ ?>
        <?php /*</div>*/ ?>

        <?php /*<div class="small-12 medium-6 columns">*/ ?>
            <?php /*<h2>Image List Lazy</h2>*/ ?>

            <?php /*<?php echo $__env->make('components.image-list-lazy', ['images' => $under_menu, 'class' => 'rotate'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>*/ ?>
        <?php /*</div>*/ ?>
    <?php /*</div>*/ ?>

    <?php /*<h2>Accordion</h2>*/ ?>

    <?php /*<?php if(isset($events)): ?>*/ ?>
        <?php /*<?php echo $__env->make('components.accordion', ['items' => $accordion], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>*/ ?>
    <?php /*<?php endif; ?>*/ ?>

    <?php /*<a class="button" onclick="$('pre.accordions').toggleClass('hide');">See Accordion Code</a>*/ ?>

    <?php /*<pre class="accordions hide" style="background: #EAEAEA; margin-bottom: 10px;">*/ ?>
    <?php /*<?php echo e(htmlspecialchars('*/ ?>
<?php /*<ul class="accordion">*/ ?>
    <?php /*<li class="is-active">*/ ?>
        <?php /*<a href="#panel1a">Accordion 1</a>*/ ?>
        <?php /*<div id="panel1a">*/ ?>
            <?php /*<p>Panel 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>*/ ?>
        <?php /*</div>*/ ?>
    <?php /*</li>*/ ?>
    <?php /*<li>*/ ?>
        <?php /*<a href="#panel2a">Accordion 2</a>*/ ?>
        <?php /*<div id="panel2a">*/ ?>
            <?php /*<p>Panel 2. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>*/ ?>
        <?php /*</div>*/ ?>
    <?php /*</li>*/ ?>
    <?php /*<li>*/ ?>
        <?php /*<a href="#panel3a">Accordion 3</a>*/ ?>
        <?php /*<div id="panel3a">*/ ?>
            <?php /*<p>Panel 3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>*/ ?>
        <?php /*</div>*/ ?>
    <?php /*</li>*/ ?>
<?php /*</ul>*/ ?>
    <?php /*')); ?>*/ ?>
    <?php /*</pre>*/ ?>

    <?php /*<h2>Table Stack</h2>*/ ?>

    <?php /*<table class="table-stack">*/ ?>
    	<?php /*<thead>*/ ?>
    		<?php /*<tr>*/ ?>
    			<?php /*<th>First Name</th>*/ ?>
    			<?php /*<th>Last Name</th>*/ ?>
    			<?php /*<th>Email</th>*/ ?>
    		<?php /*</tr>*/ ?>
    	<?php /*</thead>*/ ?>

    	<?php /*<tbody>*/ ?>
            <?php /*<?php for($i = 0; $i < 10; $i++): ?>*/ ?>
            <?php /*<tr valign="top">*/ ?>
                <?php /*<td><?php echo e($faker->firstName); ?></td>*/ ?>
                <?php /*<td><?php echo e($faker->lastName); ?></td>*/ ?>
                <?php /*<td><?php echo e($faker->email); ?></td>*/ ?>
            <?php /*</tr>*/ ?>
            <?php /*<?php endfor; ?>*/ ?>
        <?php /*</tbody>*/ ?>
    <?php /*</table>*/ ?>

    <?php /*<a class="button" onclick="$('pre.table-stack').toggleClass('hide');">See Table Code</a>*/ ?>

    <?php /*<pre class="table-stack hide" style="background: #EAEAEA; margin-bottom: 10px;">*/ ?>
    <?php /*<?php echo e(htmlspecialchars('*/ ?>
<?php /*<table class="table-stack">*/ ?>
    <?php /*<thead>*/ ?>
        <?php /*<tr>*/ ?>
            <?php /*<th></th>*/ ?>
            <?php /*<th></th>*/ ?>
            <?php /*<th></th>*/ ?>
        <?php /*</tr>*/ ?>
    <?php /*</thead>*/ ?>

    <?php /*<tbody>*/ ?>
        <?php /*<tr valign="top">*/ ?>
            <?php /*<td></td>*/ ?>
            <?php /*<td></td>*/ ?>
            <?php /*<td></td>*/ ?>
        <?php /*</tr>*/ ?>
    <?php /*</tbody>*/ ?>
<?php /*</table>*/ ?>
    <?php /*')); ?>*/ ?>
    <?php /*</pre>*/ ?>

    <?php /*<h2>Blockquote</h2>*/ ?>

    <?php /*<blockquote>*/ ?>
        <?php /*<?php echo e($faker->paragraph(10)); ?>*/ ?>
    <?php /*</blockquote>*/ ?>

    <?php /*<h2>Maginific Pop-up</h2>*/ ?>
    <?php /*<p><a href="//www.youtube.com/watch?v=guRgefesPXE"><img src="//i.wayne.edu/youtube/guRgefesPXE"></a></p>*/ ?>

    <?php /*<h1>H1: <?php echo e($faker->sentence); ?></h1>*/ ?>
    <?php /*<h2>H2: <?php echo e($faker->sentence); ?></h2>*/ ?>
    <?php /*<h3>H3: <?php echo e($faker->sentence); ?></h3>*/ ?>
    <?php /*<h4>H4: <?php echo e($faker->sentence); ?></h4>*/ ?>
    <?php /*<h5>H5: <?php echo e($faker->sentence); ?></h5>*/ ?>
    <?php /*<h6>H6: <?php echo e($faker->sentence); ?></h6>*/ ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('partials.content-area', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>