<?php
/*
* Status: Public
* Description: Lend Childpage Template
* Default: true
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LendChildpageController extends Controller
{
    /**
     * View the childpage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Show the view
        return view('lendchildpage', merge($request->data));
    }
}
