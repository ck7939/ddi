<?php
/*
* Status: Public
* Description: Lend Homepage Template
* Default: false
*/

namespace App\Http\Controllers;

use Contracts\Repositories\PromoRepositoryContract;
use Contracts\Repositories\NewsRepositoryContract;
use Contracts\Repositories\EventRepositoryContract;
use Illuminate\Http\Request;

class LendHomeController extends Controller
{
    /**
     * @param PromoRepositoryContract $promo
     * @param NewsRepositoryContract $news
     * @param EventRepositoryContract $events
     */
    public function __construct(PromoRepositoryContract $promo, NewsRepositoryContract $news, EventRepositoryContract $event)
    {
        $this->promo = $promo;
        $this->news = $news;
        $this->event = $event;
    }

    /**
     * View the homepage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $promos = $this->promo->getHomepagePromos();

        $news = $this->news->getNewsByDisplayOrder($request->data['site']['id']);

        $events = $this->event->getEvents($request->data['site']['id'] . '1599');
//dd($promos);
        return view('lendhomepage', merge($request->data),compact('news', 'events', 'promos'));
    }
}
