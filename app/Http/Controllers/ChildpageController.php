<?php
/*
* Status: Public
* Description: Childpage Template
* Default: true
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChildpageController extends Controller
{
    /**
     * View the childpage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Show the view
        return view('childpage', merge($request->data));
    }
}
