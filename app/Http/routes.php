<?php

// News Route Overrides
$app->get('{path:.*news}/category/{slug:.+}', ['middleware' => ['data', 'formy', 'spf'], 'uses' => 'NewsController@index']);
$app->get('{path:.*news}/{slug:.+}-{id:\d+}', ['middleware' => ['data', 'formy', 'spf:show'], 'uses' => 'NewsController@show']);
$app->get('{path:.*news}/', ['middleware' => ['data', 'formy', 'spf'], 'uses' => 'NewsController@index']);

// Profile Route Overrides
$app->get('{path:.*profile}/{accessid:.+}', ['middleware' => ['data', 'formy', 'spf:show'], 'uses' => 'ProfileController@show']);

// Wild Card Route
$app->addRoute(['GET', 'POST'], '{path:.*}', ['middleware' => ['data', 'formy'], function ($path, Illuminate\Http\Request $request) use ($app) {
    return app($request->controller)->index($request);
}]);
