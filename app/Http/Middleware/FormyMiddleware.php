<?php

namespace App\Http\Middleware;

use Closure;
use Waynestate\FormyParser\Parser;

class FormyMiddleware
{
    /**
     * @param Parser $parser
     */
    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->data['page']['content'] = collect($request->data['page']['content'])
            ->map(function ($content) {
                return $this->parser->parse(stripslashes($content));
            })
            ->toArray();

        return $next($request);
    }
}
