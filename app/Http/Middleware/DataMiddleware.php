<?php

namespace App\Http\Middleware;

use Closure;

class DataMiddleware
{
    /** @var $prefix **/
    protected $prefix = 'App';

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure  $next
     * @param string $path
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Set the Router Parameters to global data
        $data['parameters'] = $request->route()[2];

        // Override path depending on the need of the route
        if (! isset($data['parameters']['path']) || $data['parameters']['path'] == '') {
            $data['parameters']['path'] = $request->path();
        }

        // Get the page data
        $pageData = app($this->getPrefix().'\Repositories\PageRepository')->getRequestData($data);

        // Merge all data and page data
        $data = merge($pageData, $data);

        // Controller namespace path so it can be constructed in the routes file
        $request->controller = $this->getControllerNamespace($data['page']['controller']);

        // Set $data from all repositories
        $data = collect(glob(__DIR__.'/../../../'.strtolower($this->getPrefix()).'/Repositories/*.php'))
            ->reject(function ($filename) {
                return in_array(basename($filename), ['PageRepository.php']);
            })
            ->flatMap(function ($filename) use ($data) {
                // Make the class
                $repository = app($this->getPrefix().'\Repositories\\'.basename($filename, '.php'));

                // Get the data from the repository only if it implements the contract to do so
                if (in_array('Contracts\Repositories\DataRepositoryContract', class_implements($repository))) {
                    $repositoryData = $repository->getRequestData($data);

                    // Merge the data so it exists in one array for the view
                    $data = merge($data, $repositoryData);
                }

                return $data;
            })
            ->toArray();

        // Set the data to the request object so it gets injected into the controller
        $request->data = $data;

        return $next($request);
    }

    /**
     * Get the controller namespace.
     *
     * @param string $controller
     * @return string
     */
    public function getControllerNamespace($controller)
    {
        // First see if it exists as a prefixed controller
        if (class_exists($this->GetPrefix().'\Http\Controllers\\'.$controller)) {
            return $this->GetPrefix().'\Http\Controllers\\'.$controller;
        }

        return 'App\Http\Controllers\\'.$controller;
    }

    /**
     * Get the prefix.
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }
}
