<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Cache\Repository;

class RedisClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean out the REDIS cache for the site cache prefix';

    protected $cache;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Repository $cache)
    {
        parent::__construct();

        $this->cache = $cache;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get the keys for the site
        $keys = $this->cache->getStore()->getRedis()->keys('*');
        foreach ((array) $keys as $key) {
            $this->cache->getStore()->getRedis()->del(preg_replace('/^'.config('database.redis.options.prefix').'/', '', $key));
        }
    }
}
