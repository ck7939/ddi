<?php

namespace App\Repositories;

use Contracts\Repositories\DataRepositoryContract;
use Contracts\Repositories\PromoRepositoryContract;
use Illuminate\Cache\Repository;
use Waynestate\Api\Connector;
use Waynestate\Promotions\ParsePromos;

class PromoRepository implements DataRepositoryContract, PromoRepositoryContract
{
    /** @var Connector */
    protected $wsuApi;

    /** @var ParsePromos */
    protected $parsePromos;

    /** @var Repository */
    protected $cache;

    /**
     * @param Connector $wsuApi
     * @param ParsePromos $parsePromos
     * @param Repository $cache
     * @return void
     */
    public function __construct(Connector $wsuApi, ParsePromos $parsePromos, Repository $cache)
    {
        $this->wsuApi = $wsuApi;
        $this->parsePromos = $parsePromos;
        $this->cache = $cache;
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $data)
    {
        // Promotion groups to pull ( id => short_name )
        $group_reference = [
            3138 => 'contact',
            3139 => 'social',
            3141 => 'under_menu',
            3140 => 'hero',
            3197 => 'dditiles',
            3198 => 'lendtiles'
        ];

        // If there is an accordion custom page field and inject it into the $group_reference
        if (isset($data['data']['accordion_promo_group_id']) && $data['data']['accordion_promo_group_id'] != ''
            && ! array_key_exists($data['data']['accordion_promo_group_id'], $group_reference)
        ) {
            $group_reference[$data['data']['accordion_promo_group_id']] = 'accordion_page';
        }

        // How to parse each group after the return ( short_name => config_option )
        $group_config = [
            'contact' => 'limit:3',
            'under_menu' => 'page_id:'.$data['page']['id'],
            'hero' => 'page_id:'.$data['page']['id'].'|randomize|limit:6|first',
        ];

        // Pull all the active items from the API
        $params = [
            'method' => 'cms.promotions.listing',
            'promo_group_id' => array_keys($group_reference),
            'filename_url' => true,
            'is_active' => '1',
        ];

        // Get the raw promotions from the API and cache them
        $promos = $this->cache->remember($params['method'].md5(serialize($params)), config('cache.ttl'), function () use ($params) {
            return $this->wsuApi->sendRequest($params['method'], $params);
        });

        // Return the parsed promotions
        return $this->parsePromos->parse($promos, $group_reference, $group_config);
    }

    /**
     * {@inheritdoc}
     */
    public function getHomepagePromos()
    {
        // Promotion groups to pull ( id => short_name )
        $group_reference = [
            123 => 'example',
        ];

        // How to parse each group after the return ( short_name => config_option )
        $group_config = [
            'example' => 'first',
        ];

        // Pull all the active items from the API
        $params = [
            'method' => 'cms.promotions.listing',
            'promo_group_id' => array_keys($group_reference),
            'filename_url' => true,
            'is_active' => '1',
        ];

        // Get the raw promotions from the API
        $promos = $this->cache->remember($params['method'].md5(serialize($params)), config('cache.ttl'), function () use ($params) {
            return $this->wsuApi->sendRequest($params['method'], $params);
        });

        // Parse the promotions based on the config set
        return $this->parsePromos->parse($promos, $group_reference, $group_config);
    }
}
