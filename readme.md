### Clone for new website development
1. Create a new repository in bitbucket Owner: waynestate, Project: Client websites https://bitbucket.org/repo/create
2. Add Jenkins and Slack Webhooks to the repository you created above
    1. Go to **Settings** -> **Webhooks** -> **Add Webhook**
    2. Set the following:
        * **Name:** Push
        * **URL:** http://ci.explorewaynestate.com:8082/bitbucket-hook/
        * **Triggers:** Repository Push
    4. Save the Webhook
    5. Click **Add Webhook** again
    6. Set the following:
        * **Name:** Slack Notices
        * **URL:** https://hooks.slack.com/services/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
        * **Triggers:** Repository Push
3. Setup Jenkins jobs
    1. http://ci.explorewaynestate.com:8082/
    2. Click `New Item` on the left.
    3. Enter an item name: `{repo}-develop`, where `{repo}` is your repository name (no curly braces!).
    4. At the bottom in the copy from box, type `template-lumen-develop` and select that build.
    5. Click `Ok`.
    6. Scroll down to `Source Code Management` and change the `Repository URL` - {site_name} should be replaced with your {repo} name (no curly braces!).
    7. Click `Save` at the bottom and commit your changes with a comment.
    8. Repeat steps 2-8, but this time create a `{repo}-master` and copy from `template-lumen-master`.
3. Make sure you SSH into the vagrant box first (you might already be): `vagrant ssh`
4. `cd /vagrant/`
5. `site-setup new repository-name` where `repository-name` is the name of the repository you created.
6. Verify everything from the site-setup script ran properly. If you notice any errors please report them.
7. Now you can create feature branches and start working.

### To contribute to base
1. `cd ~/Sites/wild-wayne/ && vagrant up`
2. Type your password when prompted for it
3. `vagrant ssh`
4. `cd /vagrant/`
5. `site-setup clone base`
6. Clone the website (1561) with [http://cms.wayne.dev/repeat/new](http://cms.wayne.dev/repeat/new)
5. `make install`
6. `make build`
7. `make watch`
8. Visit [http://base.wayne.dev](http://base.wayne.dev/)

---

### Configure the site
1. Open `/config/app.php`
2. Edit the options to configure the site, some values are present in the `.env` file.
2. Server specific configuration options come from `.env` which need to be manually updated on the `dev` and `prod` servers if any changes need to occur.

### Developing global data for all views
1. Open the folder `/app/Repositories`.
2. implement the interface `DataRepositoryContract`.
3. Fill out the `getRequestData` method and return an array.

### Developing controllers
1. Open the folder `/app/Http/Controllers/`
2. This folder contains all the selectable templates from the CMS.
3. Controllers should:
    1. Dependency inject repositories into the constructor.
    2. Call repositories to obtain data to send to the view.

### Developing views
1. Open `/resources/views/`
2. This folder contains all the views for the front-end using the [blade templating engine](https://laravel.com/docs/5.2/blade)
3. Files must be saved in the format of: homepage.blade.php
4. Partials: Contains views that are only used once in the project
5. Components: Contains views that are reusable

### CMS news listing & view
1. Create a CMS page `news` in the root of the site. Select the `NewsController` as the template.
2. This will handle both the listing & view for this particular site. If you need news for a subsite, follow #1 while being in that subsite.

### CMS profile listing & view
1. Create a CMS page for the profile listing page ex: `profiles` . Select the `ProfileController` as the template.
2. Create a CMS page for the profile view, it must be: `profile` . Select the `ProfileController` as the template.
3. You can now visit `https://base.wayne.dev/profiles` and `http://base.wayne.dev/profile/{accessid}`.

---

### Style guide development for a new feature
1. Create repository contract: `contracts/Repositories`
2. Create repository: `app/Repositories`
    * implement the repository contract
3. Create fake repository: `styleguide/Repositories`
    * implement the repository contract
    * Extend the real repository from `app/Repositories`.
    * Overload any methods necessary and use or create `factories/` to get fake data.
4. Create controller
    1. Create the controller in `app/Http/Controllers`. If the styleguide needs to show variations of the feature, you may need to build controllers within `styleguide/Http/Controllers` to achieve this.
    2. Point to your view file in `resources/views`.
    3. Dependency inject the repository contract(s) into the constructor.
    4. Call whatever method(s) necessary to get data from the repository and assign it to the view.
5. Create menu item: `styleguide/Repositories/MenuRepository.php`
    1. Set `menu_item_id` and `page_id` to be the same.
    2. Follow the pattern for setting `menu_item_id`
        * Root items, increment from previous.
        * Sub items, copy parent `menu_item_id` and append 100 and auto increment from there.
    3. Set `menu_id` to 1
6. Create page: `styleguide/Pages/`
    1. Set page_id from the menu.
        * If the page needs to be full width set `page_id = null` within the menu.
        * If the page is NOT within then menu then you need to specify the path to the page. Set `var $path = '/path/to/your/page'` inside your `styleguide/Pages/` class.
    2. Set controller to the one you created in step #4.

---

### Browsersync (Automatically reload the page)
1. run `gulp watch`
2. Once completed, open up the site with port 3000 ex: https://base.wayne.dev:3000/

### To Deploy
1. `envoy run deploy` for development (develop branch) [http://www-dev.base.wayne.edu](http://www-dev.base.wayne.edu/)
2. `envoy run deploy --on=production` for production (master branch) [http://base.wayne.edu](http://base.wayne.edu/)
3. `envoy run deploy --branch=feature/feature` for development of a specific branch to the dev server

### Run tests
    phpunit

### Run test coverage
1. `make coverage`
2. Open the coverages/index.html file in your browser

### Check for outdated packages
    make status

### Update packages
    make update
